from sys import argv
import subprocess
import json
import ast
import time

instanceIds = []

def removeNesting(l):
    for i in l:
        if type(i) == list:
            removeNesting(i)
        else:
            instanceIds.append(i)

#Go to the console and create the iam role given in S3-access-policy.json. Copy and save its arn
script, role_arn = argv
role_arn_string = ""
role_arn_string = role_arn_string.join(role_arn)
#Create trust policy
create_role = subprocess.check_output('aws iam create-role --role-name S3-Access-Policy --assume-role-policy-document file://S3-access-policy-Trust-Policy.json', shell=True, stderr=subprocess.STDOUT)
iam_attach_role_cmd = 'aws iam attach-role-policy --role-name S3-access-policy --policy-arn '+role_arn_string

subprocess.run(iam_attach_role_cmd, shell=True)

# Create and add instance-profile
instance_profile_details = subprocess.check_output('aws iam create-instance-profile --instance-profile-name S3-access-policy-Instance-Profile', shell=True, stderr=subprocess.STDOUT)
# Add role to instance profile
add_role_to_instance_profile_cmd = 'aws iam add-role-to-instance-profile --role-name S3-access-policy --instance-profile-name S3-access-policy-Instance-Profile'
subprocess.run(add_role_to_instance_profile_cmd, shell= True)

#Get instance-id from console
instance_details = subprocess.check_output("aws ec2 describe-instances --query 'Reservations[*].Instances[*].InstanceId' --output json", shell=True, stderr=subprocess.STDOUT)
instance_details_string = (instance_details.decode())
instance_details_list = ast.literal_eval(instance_details_string)
removeNesting(instance_details_list)
time.sleep(10)
#Attach role to existing instances
for instanceId in instanceIds:
    attach_role_cmd = 'aws ec2 associate-iam-instance-profile --instance-id '+instanceId+' --iam-instance-profile Name=S3-access-policy-Instance-Profile'
    subprocess.run(attach_role_cmd, shell=True)
    #print(attach_role_cmd)
